module gitlab.com/bobymcbobs/url-redirector

go 1.20

require (
	github.com/gorilla/mux v1.8.0
	github.com/olekukonko/tablewriter v0.0.5
	github.com/onsi/ginkgo v1.16.4
	github.com/onsi/gomega v1.14.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/nxadm/tail v1.4.8 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	golang.org/x/net v0.0.0-20220114011407-0dd24b26b47d // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
